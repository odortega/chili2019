<?php
$lang["msg_site_name"]='Chili';
$lang['msg_version']='Version 2.0';

$lang["msg_add"]="Add";
$lang["msg_edit"]="Edit";
$lang["msg_delete"]="Delete";
$lang['msg_activate']='Activate';
$lang['msg_deactivate']='Deactivate';
$lang["msg_search"]="Search";
$lang['msg_status']='Status';
$lang['msg_reset']='Reset';
$lang['msg_save']='Save';
$lang['msg_is_slider']='Is slider';
$lang['msg_un_slider']='Un slider';

$lang['msg_table']='Table';
$lang["msg_first_name"] = "First Name";
$lang["msg_last_name"] = "Last Name";
$lang['msg_full_name']='Full Name';
$lang['msg_user_name']='User Name';
$lang["msg_dob"] = "Date of Birth";
$lang["msg_address"] = "Address";
$lang['msg_name']='Name';
$lang['msg_avatar']='Avatar';
$lang['msg_operation']='Operation';
$lang['msg_id']='Id';
$lang['msg_thumb']='Thumbnail';
$lang['msg_title']='Title';
$lang['msg_content']='content';
$lang['msg_price']='Price';
$lang['msg_images']='Images';
$lang['msg_purpose']='Purpose';
$lang['msg_condition']='Condition';
$lang['msg_new']='New';
$lang['msg_second_hand']='Second hand';
$lang['msg_sell']='Sell';
$lang['msg_buy']='Buy';
$lang['msg_post_user']='Post user';
$lang['msg_email']='Email';
$lang['msg_pwd']='Password';
$lang['msg_phone']='Phone';

/*MENU*/
$lang['msg_parent_categories']='Parent Categories';
$lang['msg_categories']='Categories';
$lang['msg_products']='Products';
$lang['msg_users']='Users';
$lang['msg_county']='County';
$lang['msg_city']='City';
$lang['msg_cities']='Cities';
$lang['copyright']='Just a Product by <a target="_blank" href="https://codecanyon.net/user/lrandom/portfolio">Lrandom</a>';
/*END MENU*/

$lang['msg_add_products']='Add New Products';
$lang['msg_add_users']='Add New User';
$lang['msg_add_categories']='Add New Categories';
$lang['msg_add_county']='Add New County';
$lang['msg_add_cities']='Add New City';
$lang['msg_edit_cities']='Edit City';
$lang['msg_edit_products']='Edit Products';
$lang['msg_edit_users']='Edit Users';
$lang['msg_edit_categories']='Edit Categories';
$lang['msg_edit_county']='Edit county';

$lang['msg_confirm_delete']='Are you sure you want to delete ?';

$lang['msg_update_profile']='Update profiles';
$lang['msg_update_pwd']='Update password';
$lang['msg_logout']='Logout';
$lang['msg_old_pwd']='Old Password';
$lang['msg_new_pwd']='New Password';
$lang['msg_cfm_pwd']='Confirm Password';
$lang['msg_hello']='Hello';
$lang['msg_settings']='Settings';
$lang['msg_fanpage']='Fanpage';
$lang['msg_currency']='Currency';
$lang['msg_logo']='Logo';


$lang['msg_admin']='Admin';
$lang['msg_staff']='Staff';
$lang['msg_user']='Users';
$lang['msg_perm']='Permision';

$lang['add_successfully']='Add successfully';
$lang['add_failed']='Add failed';
$lang['edit_successfully']='Edit successfully';
$lang['edit_failed']='Edit failed';
$lang['update_successfully']='Update successfully';
$lang['update_failed']='Update failed';

/*Upgrade 1.0.1*/
$lang['verified_msg']='This code to verified your email: ';
$lang['subject_enquiry_msg']='[ChiliMarket] Message sent via your marketplace profile from';
$lang['enquiry_msg']='just sent you a message from your marketplace profile';
$lang['response_enquiry_msg']='You can reply to this email to respond';
$lang['verified_code_label']='Verified Code';
$lang['smtp_host']='Smtp Host';
$lang['smtp_user']='Smtp User';
$lang['smtp_pwd']='Smtp Password';
$lang['smtp_port']='Smtp Port';
$lang['mailpath']='Mai path';
$lang['from_email']='From Email';
$lang['from_user']='From User';
$lang['reset_default']='Reset to Default';
$lang['msg_yes']='Yes';
$lang['msg_no']='No';
/*end*/

$lang['api_key']='Api Key';
$lang['secret_key']='Secret Key';


$lang['push']='Push';
$lang['push_content']='Push content';
$lang['push_content_placeholder']='Write your push content here';
$lang['send_push']='Send Push';
$lang['onesignal_app_id']='One Signal App Id';
$lang['onesignal_rest_api_key']='One Signal Rest Api Key';
$lang['order_confirm']='Pizza Order Confirm';
$lang['thank_for_order']='Thank you for your order, We will process it soon !!!';
$lang['msg_send']='Send';
$lang['msg_sms']='SMS';
$lang['msg_dashboard']='Dashboard';

//login 
$lang['msg_sign_in']='Sign in to start your session';
$lang['sign_in_text']='Sign In';
$lang['send_push']='Send Push';

?>