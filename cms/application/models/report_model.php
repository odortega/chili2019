<?php
class report_model extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function get($select = "*,report.id as id", $array_where = false, $array_like = false, $first = false, $offset = false, $order_by = false) {
		$data = array();
		if( $order_by != false){
			$order = key($order_by);
			if ($order != null) {
				$sort = $order_by[$order];
				$this -> db -> order_by($order, $sort);
			}
		}

		$this -> db -> select($select);
		$this -> db -> from('report');
		$this -> db -> join('products','products.id = report.product_id');
		$this -> db -> join('users','users.id = report.user_id');
		if($array_where != false)
			$this -> db -> where($array_where);
		if($array_like != false)
			$this -> db -> like($array_like);
		if($offset != false){
			$this -> db -> limit($offset, $first);
		}

		$query = $this -> db -> get();
		if ($query -> num_rows() > 0) {
			foreach ($query->result() as $rows) {
				$data[] = $rows;
			}
			foreach ($data as $r) {
				// $r->content=preg_replace('/[\r\n]+/', "",htmlspecialchars_decode($r->content));
				// $r->title=preg_replace('/[\r\n]+/', "", $r->title);
				// $r->created_at=date('M d, Y',strtotime($r->created_at));
				// $r->updated_at=date('M d, Y',strtotime($r->updated_at));
				// $r->favorites = true;
				continue;
			}
			$query -> free_result();
			return $data;
		} else {
			return null;
		}
	}

	function total($array_where, $array_like) {
		$this -> db -> select('count(*) as total');
		$this -> db -> where($array_where);
		$this -> db -> like($array_like);
		$this -> db -> from('report');
		$query = $this -> db -> get();
		$rows = $query -> result();
		$query -> free_result();
		return $rows[0] -> total;
	}

	function get_by_id($id) {
		$select = '*,report.id as id';
		$array_where = array('report.id' => $id);
		$array_like = array();
		$order_by = array();
		return $this -> get($select, $array_where, $array_like, 0, 1, $order_by);
	}

	function get_by_product_id($id) {
		$select = '*,report.id as id';
		$array_where = array('report.product_id' => $id);
		$array_like = array();
		$order_by = array();
		return $this -> get($select, $array_where, $array_like, 0, 100, $order_by);
	} 

	function get_by_user_id($id) {
		$select = '*,report.id as id';
		$array_where = array('report.user_id' => $id);
		$array_like = array();
		$order_by = array('report.created_at'=>'DESC');
		return $this -> get($select, $array_where, $array_like, 0, 100, $order_by);
	}

	function get_by_product_and_user($product_id,$user_id) {
		$select = '*,report.id as id';
		$array_where = array('report.product_id'=>$product_id,'report.user_id'=>$user_id);
		$array_like = array();
		$order_by = array();
		return $this -> get($select, $array_where, $array_like, 0, 100, $order_by);
	}

	function insert($data_array) {
		$data_array['created_at']=date('Y-m-d H:i:s');
		$data_array['updated_at']=date('Y-m-d H:i:s');
		$this -> db -> insert('report', $data_array);
		return $this -> db -> insert_id();
	}

	public function remove($arr_where) {
		$this -> db -> where($arr_where);
		$this -> db -> delete('report');
		return $this->db->affected_rows();
	}

	public function remove_by_id($id) {
		$array_where = array('id' => $id);
		return $this -> remove($array_where);
	}

	function update($data_array, $array_where) {
		$this -> db -> where($array_where);
		$this -> db -> update('report', $data_array);
	}
}
?>