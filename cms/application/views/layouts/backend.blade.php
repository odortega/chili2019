<!doctype html>
<html>
<head>
	@include('backends/commons/header')
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		@include('backends/commons/topmenu')
		
		<div class="content-wrapper">
				@yield('content')
		</div>
		
		@include('backends/commons/footer')
		@include('backends/commons/sidebar-2')
	</div>
</body>
</html>