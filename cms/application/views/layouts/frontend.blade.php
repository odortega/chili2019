<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title></title>
	@include('frontends/commons/header')

</head>

<body>
	@yield('content')
	@include('frontends/commons/footer')
</body>
</html>