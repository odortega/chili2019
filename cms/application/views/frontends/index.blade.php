<?php
$CI =& get_instance();
?>
@layout('layouts/frontend')
@section('content')
	<div class="container" id="container">
		<div class="col-md-12 col-sm-6 col-sm-offset-3 col-md-offset-0" id="content">
			
			<img src="<?php echo base_url().$obj->image_path; ?>" alt="">
			<h2><?php echo $obj->title; ?></h2>
			<div class="author">
				<img class="avt-user pull-left" src="<?php echo base_url().$obj->avt; ?>" alt="">
				<p class="user">Posted by <?php echo $obj->user_name .' at '. $obj->created_at; ?></p>
			</div>
			<p class="price">Price: <?php echo $obj->price; ?></p>
			<p class="local">Local: <?php echo $obj->cities_name.' - '.$obj->county_name; ?></p>
			<p class="order">Order: <?php echo $obj->phone; ?></p>
			<p class="content"><?php echo $obj->content; ?></p>
			
			<p class="more">More detail images</p>
			<?php if(isset($imgs)){ foreach ($imgs as $key) { ?>
				<img src="<?php echo base_url().$key->path ?>" alt="">
			<?php } }?>
			
<!-- 			<p class="install text-center">You can get the app here</p>
			<div class="store">
				<a href=""><img src="<?php //echo base_url().'statics/images/ios_store.png'; ?>" alt=""></a>
				<a href=""><img src="<?php //echo base_url().'statics/images/gg_store.png'; ?>" alt=""></a>
				<a href=""><img src="<?php //echo base_url().'statics/images/web_store.png'; ?>" alt=""></a>
			</div> -->
		</div>
	</div>
@endsection


<style>
	body{
		padding-top: 50px;
		background: #fff !important;
	}
	@media(max-width: 768px){
		.container{
			width: 100% !important;
		}
		.col-md-12{
			width: 100% !important;
			margin: 0 !important;
		}
	}
	img{
		width: 100%;
		margin: 10px 0px;
	}
	.author{
		overflow: auto;
		border-top: 1px solid #ebebeb;
		border-bottom: 1px solid #ebebeb;
		margin-bottom: 15px;
	}
	.avt-user{
		width: 35px;
		height: 35px;
		border-radius: 50%;
		margin: 5px;
		margin-left: 0;
		margin-right: 10px;
	}
	.user{
		font-weight: bold;
		color: #757575;
		margin-top: 13px;
	}
	h2{
		letter-spacing: 1px;
		text-transform: uppercase;
		font-size: 22px !important;
		font-weight: bold !important;
		margin-bottom: 30px !important;
	}
	.content{
		margin-top: 30px;
		margin-bottom: 100px;
	}
	.store{
		display: flex;
	}
	.store a{
		margin: 10px;
		display: inline-block;
	}
	.store a img{
		filter: invert();
	}
	.more, .install{
		margin-top: 50px;
		text-transform: uppercase;
		font-weight: bold;
	}
	footer{
		background: #fff;
		margin-top: 50px;
		text-align: center;
		color: #bebebe;
		padding: 10px 15px;
		padding-top: 15px;
	}
</style>