<?php
$CI =& get_instance();
?>
@layout('layouts/frontend')
@section('content')
	<div class="container-fluid" id="container">
		<div class="row">
			<div class="sidebar col-md-3">

				@include('frontends/commons/left_menu')
			</div>

			<div class="col-md-9 demo-slider">

			</div>
		</div>


		<div class="row" id="content">
			<div class="pin">
				<a>
					<h5>Cần bán ô tô ford mới</h5>
					<div class="thumb-wrapper">
						<span class="overlay-thumb"></span>
						<img class="media-object thumb" src="images/enbac.jpg">
					</div>
					<ul >
						<li>
							<a href="javascript:;"><b>Price:&nbsp</b>12/1/12</a>
						</li>
						<li>
							<a href="javascript:;"><b>Seller:&nbsp</b>Nguyen Thanh Luan</a>
						</li>
						<li>
							<a href="javascript:;"><b>County:&nbsp</b>Quang Ninh</a>
						</li>
						<li>
							<a href="javascript:;"><b>City:&nbsp</b>Ha Long</a>
						</li>
						<li>
							<a href="javascript:;"><b>Categories:&nbsp</b>Cars</a>
						</li>
					</ul>
					<hr>
					<span><a class="btn btn-primary">Follow</a></span>
					<span><a class="btn btn-default btn-details">Details</a></span>
					<span><a class="btn btn-success">Message</a></span>
				</a>
			</div>

			<div class="pin">
				<h5>Cần mua mac osx</h5>
				<div class="thumb-wrapper">
					<span class="overlay-thumb"></span>
					<img class="media-object thumb" src="images/osx.jpg">
				</div>
				<ul >
					<li>
						<a href="javascript:;"><b>Birdthay:&nbsp</b>12/1/12</a>
					</li>
					<li>
						<a href="javascript:;"><b>City:&nbsp</b>Ha Long</a>
					</li>
					<li>
						<a href="javascript:;"><b>Mt tongue:&nbsp</b>Vietnamese</a>
					</li>
					<li>
						<a href="javascript:;"><b>Join date:&nbsp</b>17/6/2013</a>
					</li>
					<li>
						<a href="javascript:;"><b>Follower:&nbsp</b>2.5k</a>
					</li>
				</ul>
				<hr>
				<span><a class="btn btn-primary">Follow</a></span>
				<span><a class="btn btn-default btn-details">Details</a></span>
				<span><a class="btn btn-success">Message</a></span>
			</div>


			<div class="pin">
				<a>
					<h5>Ngọc trinh</h5>
					<div class="thumb-wrapper">
						<span class="overlay-thumb"></span>
						<img class="media-object thumb" src="images/enbacr8.jpg">
					</div>
					<ul >
						<li>
							<a href="javascript:;"><b>Birdthay:&nbsp</b>12/1/12</a>
						</li>
						<li>
							<a href="javascript:;"><b>City:&nbsp</b>Ha Long</a>
						</li>
						<li>
							<a href="javascript:;"><b>Mt tongue:&nbsp</b>Vietnamese</a>
						</li>
						<li>
							<a href="javascript:;"><b>Join date:&nbsp</b>17/6/2013</a>
						</li>
						<li>
							<a href="javascript:;"><b>Follower:&nbsp</b>2.5k</a>
						</li>
					</ul>
					<hr>
					<span><a class="btn btn-primary">Follow</a></span>
					<span><a class="btn btn-default btn-details">Details</a></span>
					<span><a class="btn btn-success">Message</a></span>
				</a>
			</div>


			<div class="pin">
				<a>
					<h5>Ngọc trinh</h5>
					<div class="thumb-wrapper">
						<span class="overlay-thumb"></span>
						<img class="media-object thumb" src="images/enbacr8.jpg">
					</div>
					<ul >
						<li>
							<a href="javascript:;"><b>Birdthay:&nbsp</b>12/1/12</a>
						</li>
						<li>
							<a href="javascript:;"><b>City:&nbsp</b>Ha Long</a>
						</li>
						<li>
							<a href="javascript:;"><b>Mt tongue:&nbsp</b>Vietnamese</a>
						</li>
						<li>
							<a href="javascript:;"><b>Join date:&nbsp</b>17/6/2013</a>
						</li>
						<li>
							<a href="javascript:;"><b>Follower:&nbsp</b>2.5k</a>
						</li>
					</ul>
					<hr>
					<span><a class="btn btn-primary">Follow</a></span>
					<span><a class="btn btn-default btn-details">Details</a></span>
					<span><a class="btn btn-success">Message</a></span>
				</a>
			</div>


			<div class="pin">
				<a>
					<h5>Ngọc trinh</h5>
					<div class="thumb-wrapper">
						<span class="overlay-thumb"></span>
						<img class="media-object thumb" src="images/enbacr8.jpg">
					</div>
					<ul >
						<li>
							<a href="javascript:;"><b>Birdthay:&nbsp</b>12/1/12</a>
						</li>
						<li>
							<a href="javascript:;"><b>City:&nbsp</b>Ha Long</a>
						</li>
						<li>
							<a href="javascript:;"><b>Mt tongue:&nbsp</b>Vietnamese</a>
						</li>
						<li>
							<a href="javascript:;"><b>Join date:&nbsp</b>17/6/2013</a>
						</li>
						<li>
							<a href="javascript:;"><b>Follower:&nbsp</b>2.5k</a>
						</li>
					</ul>
					<hr>
					<span><a class="btn btn-primary">Follow</a></span>
					<span><a class="btn btn-default btn-details">Details</a></span>
					<span><a class="btn btn-success">Message</a></span>
				</a>
			</div>

			<div class="pin">
				<a>
					<h5>Ngọc trinh</h5>
					<div class="thumb-wrapper">
						<span class="overlay-thumb"></span>
						<img class="media-object thumb" src="images/enbacr8.jpg">
					</div>
					<ul >
						<li>
							<a href="javascript:;"><b>Birdthay:&nbsp</b>12/1/12</a>
						</li>
						<li>
							<a href="javascript:;"><b>City:&nbsp</b>Ha Long</a>
						</li>
						<li>
							<a href="javascript:;"><b>Mt tongue:&nbsp</b>Vietnamese</a>
						</li>
						<li>
							<a href="javascript:;"><b>Join date:&nbsp</b>17/6/2013</a>
						</li>
						<li>
							<a href="javascript:;"><b>Follower:&nbsp</b>2.5k</a>
						</li>
					</ul>
					<hr>
					<span><a class="btn btn-primary">Follow</a></span>
					<span><a class="btn btn-default btn-details">Details</a></span>
					<span><a class="btn btn-success">Message</a></span>
				</a>
			</div>

			<div class="pin">
				<a>
					<h5>Ngọc trinh</h5>
					<div class="thumb-wrapper">
						<span class="overlay-thumb"></span>
						<img class="media-object thumb" src="images/enbacr8.jpg">
					</div>
					<ul >
						<li>
							<a href="javascript:;"><b>Birdthay:&nbsp</b>12/1/12</a>
						</li>
						<li>
							<a href="javascript:;"><b>City:&nbsp</b>Ha Long</a>
						</li>
						<li>
							<a href="javascript:;"><b>Mt tongue:&nbsp</b>Vietnamese</a>
						</li>
						<li>
							<a href="javascript:;"><b>Join date:&nbsp</b>17/6/2013</a>
						</li>
						<li>
							<a href="javascript:;"><b>Follower:&nbsp</b>2.5k</a>
						</li>
					</ul>
					<hr>
					<span><a class="btn btn-primary">Follow</a></span>
					<span><a class="btn btn-default btn-details">Details</a></span>
					<span><a class="btn btn-success">Message</a></span>
				</a>
			</div>

			<div class="pin">
				<a>
					<h5>Ngọc trinh</h5>
					<div class="thumb-wrapper">
						<span class="overlay-thumb"></span>
						<img class="media-object thumb" src="images/enbacr8.jpg">
					</div>
					<ul >
						<li>
							<a href="javascript:;"><b>Birdthay:&nbsp</b>12/1/12</a>
						</li>
						<li>
							<a href="javascript:;"><b>City:&nbsp</b>Ha Long</a>
						</li>
						<li>
							<a href="javascript:;"><b>Mt tongue:&nbsp</b>Vietnamese</a>
						</li>
						<li>
							<a href="javascript:;"><b>Join date:&nbsp</b>17/6/2013</a>
						</li>
						<li>
							<a href="javascript:;"><b>Follower:&nbsp</b>2.5k</a>
						</li>
					</ul>
					<hr>
					<span><a class="btn btn-primary">Follow</a></span>
					<span><a class="btn btn-default btn-details">Details</a></span>
					<span><a class="btn btn-success">Message</a></span>
				</a>
			</div>

			<div class="pin">
				<a>
					<h5>Ngọc trinh</h5>
					<div class="thumb-wrapper">
						<span class="overlay-thumb"></span>
						<img class="media-object thumb" src="images/enbacr8.jpg">
					</div>
					<ul >
						<li>
							<a href="javascript:;"><b>Birdthay:&nbsp</b>12/1/12</a>
						</li>
						<li>
							<a href="javascript:;"><b>City:&nbsp</b>Ha Long</a>
						</li>
						<li>
							<a href="javascript:;"><b>Mt tongue:&nbsp</b>Vietnamese</a>
						</li>
						<li>
							<a href="javascript:;"><b>Join date:&nbsp</b>17/6/2013</a>
						</li>
						<li>
							<a href="javascript:;"><b>Follower:&nbsp</b>2.5k</a>
						</li>
					</ul>
					<hr>
					<span><a class="btn btn-primary">Follow</a></span>
					<span><a class="btn btn-default btn-details">Details</a></span>
					<span><a class="btn btn-success">Message</a></span>
				</a>
			</div>

		</div>
		
	</div>
@endsection