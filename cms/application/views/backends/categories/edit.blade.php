<?php
$CI =& get_instance();
?>
@layout('layouts/backend')
@section('content')
<section class="content-header">
  <h1>
	<?php echo lang('msg_categories'); ?>
	<small><?php echo lang('msg_edit_categories'); ?></small>
  </h1>
  <ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#"><?php echo lang('msg_dashboard'); ?></a></li>
	<li><a href="#"><?php echo lang('msg_categories'); ?></a></li>
	<li class="active"><?php echo lang('msg_edit_categories'); ?></li>
	
  </ol>
</section>

<section class="content">
    <!--show alert messager-->
    <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"><?php echo lang('msg_edit_categories'); ?></h3>
        </div>
	<?php 
	if($CI->session->flashdata('msg_ok')){
		echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>'.$CI->session->flashdata('msg_ok').'</div>';
	}
	?>
	<form class="form-horizontal" id="form" method="post" enctype="multipart/form-data" action="{{base_url().'admin/categories/edit_post'}}">
		<fieldset>

			<input type="hidden" name="id" id="id" value="{{$obj[0]->id}}">
			
			<div class="form-group">
				<label class="control-label col-sm-2" for="txtName">{{lang('msg_avatar')}}</label>
				<div class="controls col-sm-10">
					<img width="150" height="140" src="<?php echo base_url().$obj[0]->image; ?>" alt="">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-2" for="txtName">{{lang('msg_images')}}</label>
				<div class="col-sm-10">
					<input type="file" name="image" id="image">
					@if(isset($error['error_upload_file']))
					<span class="help-inline msg-error" generated="true">{{$error['error_upload_file']}}</span>
					@endif
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-2" for="txtName">{{lang('msg_name')}}</label>
				<div class="controls col-sm-10">
					<input type="text" id="name" name="name" value="{{$obj[0]->name}}" class="form-control">
					{{form_error('name')}}
				</div>
			</div>

			
			<div class="form-group">
				<label class="control-label col-sm-2" for="txtName">{{lang('msg_parent_categories')}}</label>
				<div class="col-sm-10">
					<select id="parent_id" name="parent_id" class="form-control">
						<option value="0">-----{{lang('msg_parent_categories')}}----</option>
						@foreach($parent_cat as $r)
						<option value="{{$r->id}}" <?php if($r->id==$obj[0]->parent_id){ echo 'selected="selected"';} ?>>{{$r->name}}</option>
						@endforeach
					</select>
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-10 col-sm-offset-2">
					<button type="submit" class="btn btn-primary" >
						{{lang('msg_save')}}
					</button>
				</div>
			</div>
		</fieldset>
	</form>
	<!--end form-->
</div>
	<!--end container fluid-->
	</section>
	@endsection