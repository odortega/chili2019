<?php
$CI =& get_instance();
?>
@layout('layouts/backend')
@section('content')
<section class="content-header">
  <h1>
	<?php echo lang('msg_county'); ?>
	<small><?php echo lang('msg_edit_county'); ?></small>
  </h1>
  <ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#"><?php echo lang('msg_dashboard'); ?></a></li>
	<li><a href="#"><?php echo lang('msg_county'); ?></a></li>
	<li class="active"><?php echo lang('msg_edit_county'); ?></li>
	
  </ol>
</section>

<section class="content">
    <!--show alert messager-->
    <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"><?php echo lang('msg_edit_county'); ?></h3>
        </div>
	<?php 
	if($CI->session->flashdata('msg_ok')){
		echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>'.$CI->session->flashdata('msg_ok').'</div>';
	}
	?>
	<form class="form-horizontal" id="form" method="post" action="{{base_url().'admin/county/edit_post'}}">
		<fieldset>

			<input type="hidden" name="id" id="id" value="{{$data['obj'][0]->id}}">
			<div class="form-group">
				<label class="control-label col-md-2" for="txtName">{{lang('msg_name')}}</label>
				<div class="col-md-10">
					<input type="text" id="name" name="name" class="form-control" value="{{$data['obj'][0]->name}}">
					{{form_error('name')}}
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-10 col-md-offset-2">
					<button type="submit" class="btn btn-primary" >
						{{lang('msg_save')}}
					</button>
				</div>
			</div>

		</fieldset>
	</form>
	<!--end form-->
</div>
<!--end container fluid-->
</section>
@endsection