
@layout('layouts/backend')
@section('content')
<section class="content-header">
  <h1>
   Dashboard
   <small><?php echo lang('send_push'); ?></small>
 </h1>
 <ol class="breadcrumb">
   <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
   <li class="active">Dashboard</li>
 </ol>
</section>

<section class="content">
  <div class="container-fluid">
   <!--end show alert messager-->
   <div class="row ">

    <form id="form" method="post" action="" enctype="multipart/form-data">

      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"><?php echo lang('send_push'); ?></h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form">
          <div class="box-body">
            <div class="form-group">
              <label for="push_content"><?php echo lang('push_content');?></label>
              <textarea rows="6" name="content" placeholder="<?php echo lang('push_content_placeholder'); ?>" class="form-control" id="push_content"></textarea>
            </div>

    <!--         <div class="form-group">
              <label class="control-label col-sm-2" for="txtName"><?php echo lang('msg_extras')?></label>
              <div class="col-sm-10">
                <input type="text" id="extras" class="form-control" name="extras" value="<?php echo set_value('extras')?>">
              </div>
            </div> -->

      <!--       <div style="display:block; clear:both">
              <input type="hidden" value="" name="player_ids" id="extras_list">
            </div> -->

            <div class="box-footer">
              <button type="submit" class="btn btn-primary"><?php echo lang('msg_send');?></button>
            </div>


          </form>
        </div>

      </form>
    </div>
  </div>

</section>


<script type="text/javascript" src="<?php echo base_url() ?>statics/jquery-tokeninput/jquery.tokeninput.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>statics/jquery-tokeninput/token-input.css"/>

<script>
  function removeA(arr) {
    var what, a = arguments, L = a.length, ax;
    while (L > 1 && arr.length) {
      what = a[--L];
      while ((ax= arr.indexOf(what)) !== -1) {
        arr.splice(ax, 1);
      }
    }
    return arr;
  }

  $(document).ready(function () {
    var extras=[];
    $("#extras").tokenInput(
     "<?php echo base_url() ?>api/users_api/search",
     {
      resultsFormatter: function(item) {
        var item='<li>'+item.name+'</span></li>';
        return item;
      },
      tokenFormatter:function(item){
        var item='<li>'+item.name+'</li>';
        return item;
      },
      onAdd:function(item){
        extras.push(item.player_id);
        $('#extras_list').val(extras.toString());
      },
      onDelete:function(item){
        removeA(extras,item.player_id);
        $('#extras_list').val(extras.toString());
      },
      preventDuplicates:true
    });
  });
</script>
@endsection


