<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?php echo base_url().'statics/images/user.jpg';?>" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p><?php
        if(isset($_SESSION['user'])){
          $user=$_SESSION['user'][0];
          echo $user->full_name;
        }
        ?></p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!-- search form -->
    <form action="<?php echo base_url().'admin/products/search';?>" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="query" class="form-control search-query-sidebar" placeholder="Search products...">
        <span class="input-group-btn">
          <button type="button" name="search" id="search-btn" class="btn btn-flat">
            <i class="fa fa-search"></i>
          </button>
        </span>
      </div>
    </form>
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">GENERAL</li>
      <li>
        <a href="<?php echo base_url().'admin/dashboard';?>">
          <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-wrench"></i>
          <span><?php echo lang('msg_settings')?></span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{base_url().'admin/settings/currency'}}"><i class="fa fa-circle-o"></i> {{lang('msg_currency')}}</a></li>
          <li><a href="{{base_url().'admin/settings/mail'}}"><i class="fa fa-circle-o"></i> {{lang('msg_email')}}</a></li>
          <li><a href="{{base_url().'admin/settings/push'}}"><i class="fa fa-circle-o"></i> {{lang('push')}}</a></li>
          <li><a href="{{base_url().'admin/settings/sms'}}"><i class="fa fa-circle-o"></i> {{lang('msg_sms')}}</a></li>
        </ul>
      </li>
      <li class="header">TAXONOMY</li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-tags"></i>
          <span><?php echo lang('msg_categories')?></span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo base_url().'admin/categories'?>"><i class="fa fa-circle-o"></i> <?php echo lang('msg_categories')?></a></li>
          <li><a href="<?php echo base_url().'admin/categories/create'?>"><i class="fa fa-circle-o"></i> <?php echo lang('msg_add_categories')?></a></li>
        </ul>
      </li>
<!--       <li class="treeview">
        <a href="#">
          <i class="fa fa-globe"></i>
          <span><?php //echo lang('msg_county')?></span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php //echo base_url().'admin/county'?>"><i class="fa fa-circle-o"></i> <?php //echo lang('msg_county')?></a></li>
          <li><a href="<?php //echo base_url().'admin/county/create'?>"><i class="fa fa-circle-o"></i> <?php //echo lang('msg_add_county')?></a></li>
        </ul>
      </li> -->
<!--       <li class="treeview">
        <a href="#">
          <i class="fa fa-map-marker"></i>
          <span><?php //echo lang('msg_city')?></span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php //echo base_url().'admin/cities'?>"><i class="fa fa-circle-o"></i> <?php //echo lang('msg_city')?></a></li>
          <li><a href="<?php //echo base_url().'admin/cities/create'?>"><i class="fa fa-circle-o"></i> <?php //echo lang('msg_add_cities')?></a></li>
        </ul>
      </li> -->
      
      <li class="header">LIST</li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-edit"></i>
          <span><?php echo lang('msg_products')?></span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo base_url().'admin/products'?>"><i class="fa fa-circle-o"></i> <?php echo lang('msg_products')?></a></li>
          <!--      <li><a href="<?php //echo base_url().'admin/products/create'?>"><i class="fa fa-circle-o"></i> <?php //echo lang('msg_add_products')?></a></li> -->
        </ul>
      </li>
      
      <li class="header">USER</li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-users"></i>
          <span><?php echo lang('msg_users')?></span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo base_url().'admin/users'?>"><i class="fa fa-circle-o"></i> <?php echo lang('msg_users')?></a></li>
          <li><a href="<?php echo base_url().'admin/users/create'?>"><i class="fa fa-circle-o"></i> <?php echo lang('msg_add_users')?></a></li>
        </ul>
      </li>

      <li class="header">MORE</li>
      <li class="bg-green">
        <a href="https://codecanyon.net/user/lrandom/portfolio">
          <i class="fa fa-gift"></i>
          <span>Lrandom</span>
        </a>
      </li>

      <li>
       <a href="https://lrandomdev.com/docs">
        <i class="fa fa-book"></i> 
        <span>online documentation</span>
      </a>
    </li>

    <li>
     <a href="https://codecanyon.net/item/chili-ionic-3-marketplaces-app-with-admin-panel/20528089">
      <i class="fa fa-shopping-cart"></i> <span>buy now</span>
    </a>
  </li>
</ul>
</section>
<!-- /.sidebar -->
</aside>