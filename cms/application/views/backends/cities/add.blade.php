<?php
$CI =& get_instance();
?>
@layout('layouts/backend')
@section('content')
<script type="text/javascript">
jQuery(document).ready(function($) {
	$('#name').focus();
});
</script>
<section class="content-header">
  <h1>
	<?php echo lang('msg_city'); ?>
	<small><?php echo lang('msg_add_cities'); ?></small>
  </h1>
  <ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#"><?php echo lang('msg_dashboard'); ?></a></li>
	<li><a href="#"><?php echo lang('msg_city'); ?></a></li>
	<li class="active"><?php echo lang('msg_add_cities'); ?></li>
	
  </ol>
</section>

<section class="content">
    <!--show alert messager-->
    <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"><?php echo lang('msg_add_cities'); ?></h3>
        </div>
		
	<?php 
	if($CI->session->flashdata('msg_ok')){
		echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>'.$CI->session->flashdata('msg_ok').'</div>';
	}
	?>
	<form class="form-horizontal" id="form" method="post">
		<fieldset>

			<div class="form-group">
				<label class="control-label col-md-2" for="txtName">{{lang('msg_name')}}</label>
				<div class="col-md-10">
					<input type="text" id="name" name="name" class="form-control" >
					{{form_error('name')}}
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-2" for="txtName">{{lang('msg_county')}}</label>
				<div class="col-sm-10">
					<select id="county" name="county" class="form-control">
						<option value="">-----{{lang('msg_county')}}----</option>
						@foreach($county as $r)
						<option value="{{$r->id}}">{{$r->name}}</option>
						@endforeach
					</select>
					{{form_error('county')}}
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-10 col-md-offset-2">
					<button type="submit" class="btn btn-primary" >
						{{lang('msg_save')}}
					</button>
					<input type="reset" class="btn" value="{{lang('msg_reset')}}"/>
				</div>
			</div>

		</fieldset>
	</form>
	<!--end form-->
</div>
<!--end container fluid-->
</section>
@endsection