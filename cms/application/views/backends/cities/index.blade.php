@layout('layouts/backend')
@section('content')

<section class="content-header">
  <h1>
	<?php echo lang('msg_dashboard'); ?>
	<small><?php echo lang('msg_city'); ?></small>
  </h1>
  <ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#"><?php echo lang('msg_dashboard'); ?></a></li>
	<li class="active"><?php echo lang('msg_city'); ?></li>
  </ol>
</section>

<div class="content">
	
	<div class="page-header controls-wrapper">
		<a href="{{base_url().'admin/county/create'}}" class="btn btn-primary">{{lang('msg_add')}}</a>
	</div>
	
	<!--show alert messager-->
	<h4>
		@if(isset($data['search_title']))
		   {{$data['search_title']}}
		@endif
	</h4>
	<!--end show alert messager-->
    <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title"><?php echo lang('msg_city'); ?></h3>

          <div class="box-tools">
            <div class="input-group input-group-sm" style="width: 150px;">
              <input type="text" name="table_search" class="search-query form-control pull-right" placeholder="<?php echo lang('msg_search');?>" value="<?php echo (isset($_GET['query'])) ? $_GET['query'] : ''; ?>">
              
              	<script type="text/javascript">
              	$('.search-query').keypress(function(e) {
              		var code = (e.keyCode ? e.keyCode : e.which);
              		if (code == 13) {
              			var q = $('.search-query').val();
              			if (q != "") {
              				location.href ="<?php echo base_url().'admin/county/search';?>?query=" + q;
              			}
              		}
              	})
              	</script>
             
              <div class="input-group-btn">
                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
              </div>
            </div>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
          <table class="table table-hover">
            <tbody>
            <tr>
              <th width="100px" style="text-align:center"><a href="">{{lang('msg_id')}}</a></th>
              <th>{{lang('msg_city')}}</th>
              <th width="150px">{{lang('msg_operation')}}</th>
            </tr>
            
            @if($data['list']!=null)
            @foreach($data['list'] as $r)
            <tr>
                <td style="text-align:center;">{{$r->id}}</td>
                <td>{{$r->name}}</td>
                <td>
                    <a class="btn btn-info"  href="{{base_url().'admin/cities/edit_get?id='.$r->id}}">{{lang('msg_edit')}}</a> 
                    <a class="btn btn-danger" href="{{base_url().'admin/cities/delete?id='.$r->id}}" onclick="return confirm('{{lang('msg_confirm_delete')}}')">{{lang('msg_delete')}}</a>
                </td>
            </tr>
            @endforeach
            @endif
          </tbody></table>
          <center>{{$data['page_link']}}</center>
        </div>
        <!-- /.box-body -->
      </div>
</div>

@endsection
