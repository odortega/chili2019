<?php
$CI =& get_instance();
?>
@layout('layouts/backend')
@section('content')


<script type="text/javascript" src="{{base_url()}}statics/tinymce/jquery.tinymce.js"></script>

<section class="content-header">
  <h1>
	<?php echo lang('msg_products'); ?>
	<small><?php echo lang('msg_add_products'); ?></small>
  </h1>
  <ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#"><?php echo lang('msg_dashboard'); ?></a></li>
	<li><a href="#"><?php echo lang('msg_products'); ?></a></li>
	<li class="active"><?php echo lang('msg_add_products'); ?></li>
	
  </ol>
</section>

<section class="content">
    <!--show alert messager-->
    <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"><?php echo lang('msg_add_products'); ?></h3>
        </div>
		
	<div>
		<?php 
		if($CI->session->flashdata('msg_ok')){
			echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>'.$CI->session->flashdata('msg_ok').'</div>';
		}
		?>
	</div>

	<form class="form-horizontal" id="form" method="post" action="" enctype="multipart/form-data">
		<fieldset>

			<div class="form-group">
				<label class="control-label col-sm-2">{{lang('msg_title')}}</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="title" name="title" value="{{set_value('title')}}">
					{{form_error('title')}}
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-2" for="txtName">{{lang('msg_price')}}</label>
				<div class="col-sm-10">
					<input type="text" id="price" class="form-control" name="price" value="{{set_value('price')}}">
					{{form_error('price')}}
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-2" for="txtName">{{lang('msg_categories')}}</label>
				<div class="controls col-md-9 multil_select">
					<?php $CI =& get_instance();
						$CI->load->model('categories_model');
						function show_cat($parent_id){
							$CI =& get_instance();
							$cat=$CI->categories_model->get('*',array('parent_id'=>$parent_id));
							if($cat!=null){
								foreach ($cat as $r) {
									echo '<div class="checkbox">
									<label><input type="checkbox" class="types" name="categories[]" value="'.$r->id.'">'.$r->name.'</label>';
									show_cat($r->id);
									echo '</div>';
								}
							}
						}
						show_cat(0);
					?>
					{{form_error('categories')}}
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-2" for="txtName">{{lang('msg_content')}}</label>
				<div class="col-sm-10">
					<textarea id="content" name="content" rows="7" class="form-control">{{set_value('content')}}</textarea>
					{{form_error('content')}}
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-2" for="txtName">{{lang('msg_images')}}</label>
				<div class="col-sm-10">
					<input type="file" name="image" id="image">
					@if(isset($error['error_upload_file']))
					<span class="help-inline msg-error" generated="true">{{$error['error_upload_file']}}</span>
					@endif
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-2" for="txtName"></label>
				<div class="col-sm-10">
					<input type="file" name="image1" id="image1">
					@if(isset($error['error_upload_file']))
					<span class="help-inline msg-error" generated="true">{{$error['error_upload_file_1']}}</span>
					@endif
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-2" for="txtName"></label>
				<div class="col-sm-10">
					<input type="file" name="image2" id="image2">
					@if(isset($error['error_upload_file']))
					<span class="help-inline msg-error" generated="true">{{$error['error_upload_file_2']}}</span>
					@endif
				</div>
			</div>


			<div class="col-sm-10 col-sm-offset-2">
				<button type="submit" class="btn btn-primary" >
					{{lang('msg_save')}}
				</button>
				<button type="reset" class="btn"> 
					{{lang('msg_reset')}}
				</button>
			</div>

		</fieldset>
	</form>

	<!--end form-->

	<!--end container fluid-->
	<script>
		jQuery(document).ready(function($) {
			$('#provinces').change(function(){
				$county_id=$(this).val();
				$.ajax({
					url: '{{base_url()}}admin/cities/get_city',
					type: 'POST',
					dataType: 'html',
					data: {county_id: $county_id },
				})
				.done(function() {
					console.log("success");
				})
				.fail(function() {
					console.log("error");
				})
				.always(function(data) {
					$('#cities').html(data);
				});
			})

		});
	</script>
</div>

</section>
@endsection
