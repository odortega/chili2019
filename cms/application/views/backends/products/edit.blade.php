<?php
$CI =& get_instance();
?>

@layout('layouts/backend')
@section('content')

<div class="container-fluid wrapper">
	<?php 
	if($CI->session->flashdata('msg_ok')){
		echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>'.$CI->session->flashdata('msg_ok').'</div>';
	}
	?>
	<form class="form-horizontal" id="form" enctype="multipart/form-data" method="post" action="<?php echo base_url().'admin/products/edit_post';?>">
		<fieldset>
			<legend>
				<?php echo lang('msg_edit_product');?>
			</legend>

			<input type="hidden" name="id" id="id" value="<?php echo $data['obj'][0]->id;?>">
			
			
			<div class="">
				<div class="form-group" style="margin-bottom: 30px;">
					<label class="control-label col-xs-2" for="txtName"><?php echo lang('msg_thumb');?> 1</label>
					<div class="col-xs-2">
						<img style="min-width: 80px; background: #ebebeb; max-width: 100px; min-height: 100px; height: 100px;  margin-bottom: 10px;" src="<?php echo base_url().$data['thumb'][0]->path; ?>" alt="">
						<input type="file" name="photo1" class="form-control">
						<?php echo form_error('photo1');?>
					</div>

					<label class="control-label col-xs-1" for="txtName"><?php echo lang('msg_thumb');?> 2</label>
					<div class="col-xs-2">
						<img style="min-width: 80px; background: #ebebeb; max-width: 100px; min-height: 100px; height: 100px; margin-bottom: 10px;" src="<?php if(isset($data['thumb'][1])){ echo base_url().$data['thumb'][1]->path; }else{ echo base_url().''; } ?>" alt="">
						<input type="file" name="photo2" class="form-control">
						<?php echo form_error('photo2');?>
					</div>

					<label class="control-label col-xs-1" for="txtName"><?php echo lang('msg_thumb');?> 3</label>
					<div class="col-xs-2">
						<img style="min-width: 80px; background: #ebebeb; max-width: 100px; min-height: 100px; height: 100px; margin-bottom: 10px;" src="<?php if(isset($data['thumb'][2])){ echo base_url().$data['thumb'][2]->path; }else{ echo base_url().''; } ?>" alt="">
						<input type="file" name="photo3" class="form-control">
						<?php echo form_error('photo3');?>
					</div>
				</div>
			</div>




			<div class="form-group">
				<label class="control-label col-xs-2" for="txtName"><?php echo lang('msg_title');?></label>
				<div class="col-xs-10">
					<input type="text" name="title" class="form-control" value="<?php echo $data['obj'][0]->title;?>">
					<?php echo form_error('title');?>
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-xs-2" for="txtName"><?php echo lang('msg_content');?></label>
				<div class="col-xs-10">
					<textarea name="content" rows="5" class="form-control"><?php echo $data['obj'][0]->content;?></textarea>
					<?php echo form_error('content');?>
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-xs-2" for="txtName"><?php echo lang('msg_price');?></label>
				<div class="col-xs-10">
					<input type="number" name="price" class="form-control">
					<?php echo form_error('price');?>
				</div>
			</div>

			<div class="form-group">
				<div class="col-xs-10 col-xs-offset-2">
					<button type="submit" class="btn btn-primary" >
						<?php echo lang('msg_save');?>
					</button>
				</div>
			</div>

		</fieldset>
	</form>

	<!--end row fluid-->
@endsection