@layout('layouts/backend')
@section('content')

<section class="content-header">
  <h1>
	<?php echo lang('msg_dashboard'); ?>
	<small><?php echo lang('msg_products'); ?></small>
  </h1>
  <ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#"><?php echo lang('msg_dashboard'); ?></a></li>
	<li class="active"><?php echo lang('msg_products'); ?></li>
  </ol>
</section>

<div class="content">
	
	<!-- <div class="page-header controls-wrapper">
		<a href="{{base_url().'admin/products/create'}}" class="btn btn-primary">{{lang('msg_add')}}</a>
	</div> -->
	
	<!--show alert messager-->
	<h4>
		@if(isset($data['search_title']))
		   {{$data['search_title']}}
		@endif
	</h4>
	<!--end show alert messager-->
    <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title"><?php echo lang('msg_products'); ?></h3>

          <div class="box-tools">
            <div class="input-group input-group-sm" style="width: 150px;">
              <input type="text" name="table_search" class="search-query form-control pull-right" placeholder="<?php echo lang('msg_search');?>" value="<?php echo (isset($_GET['query'])) ? $_GET['query'] : ''; ?>">
              
              	<script type="text/javascript">
              	$('.search-query').keypress(function(e) {
              		var code = (e.keyCode ? e.keyCode : e.which);
              		if (code == 13) {
              			var q = $('.search-query').val();
              			if (q != "") {
              				location.href ="<?php echo base_url().'admin/products/search';?>?query=" + q;
              			}
              		}
              	})
              	</script>
             
              <div class="input-group-btn">
                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
              </div>
            </div>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding" style="padding-bottom: 100px!important">
          <table class="table table-hover">
            <tbody>
            <tr>
              <th width="100px" style="text-align:center"><a href="">{{lang('msg_id')}}</a></th>
              <th width="50px">{{lang('msg_thumb')}}</th>
              <th>{{lang('msg_title')}}</th>
              <th>{{lang('msg_content')}}</th>
              <th>{{lang('msg_price')}}</th>
              <th>{{lang('msg_post_user')}}</th>
              <th>{{lang('msg_status')}}</th>
              <th width="150px">{{lang('msg_operation')}}</th>
            </tr>
            
            @if($data['list']!=null)
            @foreach($data['list'] as $r)
            <tr>
                <td style="text-align:center;">{{$r->id}}</td>
                <?php
                if($r->image_path!=null){
                    ?>
                    <td><img class="thumbnail" src='{{base_url().$r->image_path}}' alt="{{$r->title}}"   style="width: 100%; max-height: 100px; margin: 0" /></td>
                    <?php }else{ ?>
                    <td><img class="thumbnail" src='{{base_url()."statics/images/no_photo.png"}}' alt="{{$r->title}}" style="width: 100%; max-height: 100px; margin: 0" /></td>
                    <?php } ?>
                    <td>{{$r->title}}</td>
                    <td>{{$r->content}}</td>
                    <td>{{$r->price}}</td>
                    <td>{{$r->email}}</td>
                    <td><?php
                    if($r->activated == 1){
                        echo '<span class="label label-success" >'.lang('msg_activate').'</span>';
                    }else{
                        echo '<span class="label label-danger">'.lang('msg_deactivate').'</span>';
                    }
                    if($r->is_slider == 1){
                        echo '<span style="margin: 3px;" class="label label-danger" >'.lang('msg_is_slider').'</span>';
                    }
                    ?></td>
                <td>
                    <div class="btn-group">
                        <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" href="#">
                            {{lang('msg_operation')}}
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" style="right:0; left: auto">
                            <li><a href="{{base_url().'index.php/admin/products/lock?id='.$r->id}}" >{{lang('msg_deactivate')}}</a></li>
                            <li><a href="{{base_url().'index.php/admin/products/activate?id='.$r->id}}">{{lang('msg_activate')}}</a></li>

                            <li>
                                <?php if($r->is_slider != 1){ ?>
                                    <a href="{{base_url().'index.php/admin/products/is_slider?id='.$r->id}}">{{lang('msg_is_slider')}}</a>
                                <?php }else{ ?>
                                    <a href="{{base_url().'index.php/admin/products/un_slider?id='.$r->id}}">{{lang('msg_un_slider')}}</a>
                                <?php } ?>
                            </li>
                              <li><a href="{{base_url().'index.php/admin/products/edit_get?id='.$r->id}}">{{lang('msg_edit')}}</a></li>
                            <li><a href="{{base_url().'index.php/admin/products/delete?id='.$r->id}}" onclick="return confirm('{{lang('msg_confirm_delete')}}')">{{lang('msg_delete')}}</a></li>
                        </ul>
                    </div>
                </td>
            </tr>
            @endforeach
            @endif
          </tbody></table>
          <center>{{$data['page_link']}}</center>
        </div>
        <!-- /.box-body -->
      </div>
</div>

@endsection
