@layout('layouts/backend')
@section('content')

<section class="content-header">
  <h1>
	<?php echo lang('msg_settings'); ?>
	<small><?php echo lang('msg_sms'); ?></small>
  </h1>
  <ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#"><?php echo lang('msg_dashboard'); ?></a></li>
    <li><a href="#"><?php echo lang('msg_settings'); ?></a></li>
	<li class="active"><?php echo lang('msg_sms'); ?></li>
  </ol>
</section>

<section class="content">
    <!--show alert messager-->
    <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"><?php echo lang('msg_sms'); ?></h3>
        </div>

	<div style="display: block;position: relative;overflow: hidden;">
		<div class="col-md-12">
            <div class="form-group">
              <label style="margin-top: 20px;">What send sms method you prefer ?</label>
              <select name="prefer" class="prefer form-control">
                  <option value="0" <?php if($obj['sms_prefer']=='0'){echo 'selected';} ?>>
                      Elastice SMS
                  </option>
                  <option value="1"  <?php if($obj['sms_prefer']=='1'){echo 'selected';} ?>>
                      NextMo
                  </option>
      			</select>
            </div>

			<div style="margin-top: 10px">
				<!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active">
						<a href="#tab-1" aria-controls="tab-1" role="tab" data-toggle="tab">
						Elastice SMS Service
						</a>
					</li>
					<li role="presentation">
						<a href="#tab-2" aria-controls="tab-1" role="tab" data-toggle="tab">
						NextMo
						</a>
					</li>
				</ul>
				<!-- Tab panes -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="tab-1">
						<form class="form-horizontal"  style="margin-top: 10px"  id="form" method="post" action="" enctype="multipart/form-data">

							<fieldset>
								<div class="form-group">
									<label class="control-label col-md-2" for="txtName">{{lang('api_key')}}</label>
									<div class="controls col-md-10">
										<input type="text" class="form-control" name="elastice_api_key" value="{{$obj['elastice_api_key']}}">
									</div>
								</div>

								<div class="form-group">
									<div class="col-md-10 col-md-offset-2">
										<button type="submit" class="btn btn-primary" >
											{{lang('msg_save')}}
										</button>
										<a href="{{base_url()}}admin/settings/reset_elastice_settings_2" 
										class="btn btn-default">
										{{lang('reset_default')}}
									</a>
								</div>
							</div>
						</fieldset>
					</form>
				</div>

				<div role="tabpanel" class="tab-pane" id="tab-2">
					<form class="form-horizontal"  style="margin-top: 10px"  id="form" method="post" action="" enctype="multipart/form-data">
						
						<fieldset>
							<div class="form-group">
								<label class="control-label col-md-2" for="txtName">{{lang('api_key')}}</label>
								<div class="controls col-md-10">
									<input type="text" class="form-control" name="nexmo_api_key" value="{{$obj['nexmo_api_key']}}">
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-2" for="txtName">{{lang('secret_key')}}</label>
								<div class="controls col-md-10">
									<input type="text" class="form-control" name="nexmo_secret_key" value="{{$obj['nexmo_secret_key']}}">
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-10 col-md-offset-2">
									<button type="submit" class="btn btn-primary" >
										{{lang('msg_save')}}
									</button>
									<a href="{{base_url()}}admin/settings/reset_sms_settings" 
									class="btn btn-default">
									{{lang('reset_default')}}
								</a>
							</div>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>
</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('.prefer').change(function(){
			location.href='<?php echo base_url()."admin/settings/sms_prefer_settings?prefer="; ?>'+$(this).val();
		})
	})
</script>
@endsection

