@layout('layouts/backend')
@section('content')

<section class="content-header">
  <h1>
	<?php echo lang('msg_settings'); ?>
	<small><?php echo lang('msg_email'); ?></small>
  </h1>
  <ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#"><?php echo lang('msg_dashboard'); ?></a></li>
    <li><a href="#"><?php echo lang('msg_settings'); ?></a></li>
	<li class="active"><?php echo lang('msg_email'); ?></li>
  </ol>
</section>

<section class="content">
    <!--show alert messager-->
    <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"><?php echo lang('msg_email'); ?></h3>
        </div>

	<div style="display: block;position: relative;overflow: hidden;">
		<div class="col-md-12">
            
            <div class="form-group">
              <label style="margin-top: 20px;">What send email method you prefer ?</label>
              <select name="prefer" class="prefer form-control">
      				<option value="0" <?php if($obj['mail_prefer']=='0'){echo 'selected';} ?>>
      					SMTP
      				</option>
      				<option value="1"  <?php if($obj['mail_prefer']=='1'){echo 'selected';} ?>>
      					Elastice Mail Service
      				</option>
      			</select>
            </div>
			
			

			<div style="margin-top: 10px">
				<!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active">
						<a href="#general" aria-controls="general" role="tab" data-toggle="tab">General</a>
					</li>

					<li role="presentation">
						<a href="#home" aria-controls="home" role="tab" data-toggle="tab">SMTP</a>
					</li>

					<li role="presentation">
						<a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Elastice Mail Service</a>
					</li>
				</ul>
				<!-- Tab panes -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="general">
						<form class="form-horizontal" style="margin-top: 10px" id="form" method="post" action="" enctype="multipart/form-data">
							<fieldset>
								<div class="form-group">
									<label class="control-label col-md-2" >
										{{lang('from_user')}}
									</label>
									<div class="controls col-md-10">
										<input type="text" class="form-control" name="from_user" value="{{$obj['from_user']}}">
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-2" for="txtName">{{lang('from_email')}}</label>
									<div class="controls col-md-10">
										<input type="text" class="form-control" name="from_email" value="{{$obj['from_email']}}">
									</div>
								</div>

								<div class="form-group">
									<div class="col-md-10 col-md-offset-2">
										<button type="submit" class="btn btn-primary" >
											{{lang('msg_save')}}
										</button>
										<a href="{{base_url()}}admin/settings/reset_general_mail_settings" class="btn btn-default">
											{{lang('reset_default')}}
										</a>
									</div>
								</div>
							</fieldset>
						</form>
					</div>

					<div role="tabpanel" class="tab-pane" id="home">
						<form class="form-horizontal" style="margin-top: 10px" id="form" method="post" action="" enctype="multipart/form-data">
							<fieldset>
								
								<div class="form-group">
									<label class="control-label col-md-2" for="txtName">{{lang('smtp_host')}}</label>
									<div class="controls col-md-10">
										<input type="text" id="host" class="form-control" name="host" value="{{$obj['smtp_host']}}">
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-2" for="txtName">{{lang('smtp_user')}}</label>
									<div class="controls col-md-10">
										<input type="text" id="user" class="form-control" name="user" value="{{$obj['smtp_user']}}">
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-2" for="txtName">{{lang('smtp_pwd')}}</label>
									<div class="controls col-md-10">
										<input type="password" id="pwd" class="form-control" name="pwd" value="{{$obj['smtp_pass']}}">
									</div>
								</div>


								<div class="form-group">
									<label class="control-label col-md-2" for="txtName">{{lang('smtp_port')}}</label>
									<div class="controls col-md-10">
										<input type="text" id="port" class="form-control" name="port" value="{{$obj['smtp_port']}}">
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-2" for="txtName">{{lang('mailpath')}}</label>
									<div class="controls col-md-10">
										<input type="text" id="mail_path" class="form-control" name="mail_path" value="{{$obj['mailpath']}}">
									</div>
								</div>

								<div class="form-group">
									<div class="col-md-10 col-md-offset-2">
										<button type="submit" class="btn btn-primary" >
											{{lang('msg_save')}}
										</button>
										<a href="{{base_url()}}admin/settings/reset_smtp_settings" class="btn btn-default">
											{{lang('reset_default')}}
										</a>
									</div>
								</div>
							</fieldset>
						</form>
						<!--end form-->
					</div>

					<div role="tabpanel" class="tab-pane" id="profile">
						<form class="form-horizontal"  style="margin-top: 10px"  id="form" method="post" action="" enctype="multipart/form-data">
							
							<fieldset>
								<div class="form-group">
									<label class="control-label col-md-2" for="txtName">{{lang('api_key')}}</label>
									<div class="controls col-md-10">
										<input type="text" class="form-control" name="nextmo_api_key" value="{{$obj['elastice_api_key']}}">
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-2" for="txtName">{{lang('secret_key')}}</label>
									<div class="controls col-md-10">
										<input type="text" class="form-control" name="nextmo_secret_key" value="{{$obj['elastice_api_key']}}">
									</div>
								</div>

								<div class="form-group">
									<div class="col-md-10 col-md-offset-2">
										<button type="submit" class="btn btn-primary" >
											{{lang('msg_save')}}
										</button>
										<a href="{{base_url()}}admin/settings/reset_elastice_settings" 
										class="btn btn-default">
										{{lang('reset_default')}}
									</a>
								</div>
							</div>
						</fieldset>
					</form>
					<!--end form-->
				</div>
			</div>
		</div>
	</div>
</div>
</div>

</section>

<script type="text/javascript">
	$(document).ready(function(){
		$('.prefer').change(function(){
			location.href='<?php echo base_url()."admin/settings/mail_prefer_settings?prefer="; ?>'+$(this).val();
		})
	})
</script>
@endsection

