<?php
$CI =& get_instance();
?>
@layout('layouts/backend')
@section('content')

<section class="content-header">
  <h1>
	<?php echo lang('msg_settings'); ?>
	<small><?php echo lang('msg_currency'); ?></small>
  </h1>
  <ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#"><?php echo lang('msg_dashboard'); ?></a></li>
    <li><a href="#"><?php echo lang('msg_settings'); ?></a></li>
	<li class="active"><?php echo lang('msg_currency'); ?></li>
  </ol>
</section>

<section class="content">
    <!--show alert messager-->
    <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"><?php echo lang('msg_currency'); ?></h3>
        </div>
		
	<?php 
	if($CI->session->flashdata('msg_ok')){
		echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>'.$CI->session->flashdata('msg_ok').'</div>';
	}
	?>
	<form class="form-horizontal" id="form" method="post" action="" enctype="multipart/form-data">
		<fieldset>

			<div class="form-group">
				<label class="control-label col-md-2" for="txtName">{{lang('msg_currency')}}</label>
				<div class="col-md-10">
					<select name="currency" class="form-control">
						@if($list!=null)
						@foreach($list as $r)
						<option value="{{$r->currency_code}}">{{$r->currency_code}}&nbsp;({{$r->name}})</option>
						@endforeach
						@endif
					</select>
				</div>
			</div>

			</hr>

			<div class="form-group">
				<div class="col-md-10 col-md-offset-2">
					<button type="submit" class="btn btn-primary">
						{{lang('msg_save')}}
					</button>
				</div>
			</div>

		</fieldset>
	</form>
	<!--end form-->
</div>
<!--end container fluid-->

</section>

@endsection