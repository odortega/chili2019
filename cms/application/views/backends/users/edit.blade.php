<?php
$CI =& get_instance();
?>
<!--end container fluid-->
@endsection@layout('layouts/backend')
@section('content')
<section class="content-header">
  <h1>
	<?php echo lang('msg_users'); ?>
	<small><?php echo lang('msg_edit_users'); ?></small>
  </h1>
  <ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#"><?php echo lang('msg_dashboard'); ?></a></li>
	<li><a href="#"><?php echo lang('msg_users'); ?></a></li>
	<li class="active"><?php echo lang('msg_edit_users'); ?></li>
	
  </ol>
</section>

<section class="content">
    <!--show alert messager-->
    <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"><?php echo lang('msg_edit_users'); ?></h3>
        </div>
		
		
	<?php 
	if($CI->session->flashdata('msg_ok')){
		echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>'.$CI->session->flashdata('msg_ok').'</div>';
	}
	?>
	<form class="form-horizontal" id="form" method="post" action="{{base_url().'admin/users/edit_post'}}" enctype="multipart/form-data">
		<fieldset>
			<input type="hidden" name="id" id="id" value="{{$data['obj'][0]->id}}">
			<div class="form-group">
				<label class="control-label col-md-2" for="txtName">UserName</label>
				<div class="col-md-10">
					<input type="text" id="user_name" class="form-control" name="user_name" value="{{$data['obj'][0]->user_name}}">
					{{form_error('user_name')}}
				</div>
			</div>


			<div class="form-group">
				<label class="control-label col-md-2" for="txtName">Full Name</label>
				<div class="col-md-10">
					<input type="text" id="full_name" class="form-control" name="full_name"  value="{{$data['obj'][0]->full_name}}">
					{{form_error('full_name')}}
				</div>
			</div>


			<div class="form-group">
				<label class="control-label col-md-2" for="txtName">Email</label>
				<div class="col-md-10">
					<input type="text" id="email" class="form-control" name="email" value="{{$data['obj'][0]->email}}">
					{{form_error('email')}}
				</div>
			</div>


			<div class="form-group">
				<label class="control-label col-md-2" for="txtName">Address</label>
				<div class="col-md-10">
					<input type="text" id="address" class="form-control" name="address" value="{{$data['obj'][0]->address}}">
					{{form_error('address')}}
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-2" for="txtName">{{lang('msg_perm')}}</label>
				<div class="col-md-10">
					<select name="perm" class="form-control">
						<option value="{{STAFF}}">{{lang('msg_staff')}}</option>
						<option value="{{ADMIN}}">{{lang('msg_admin')}}</option>
						<option value="2">shopper</option>
						<option value="3">vendor</option>
					</select>
					{{form_error('perm')}}
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-10 col-md-offset-2">
					<button type="submit" class="btn btn-primary" >
						{{lang('msg_save')}}
					</button>
				</div>
			</div>
		</fieldset>
	</form>
	<!--end form-->
</div>
<!--end container fluid-->
</section>
@endsection