@layout('layouts/backend')
@section('content')

<section class="content-header">
  <h1>
	<?php echo lang('msg_users'); ?>
	<small><?php echo lang('msg_update_profile'); ?></small>
  </h1>
  <ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#"><?php echo lang('msg_dashboard'); ?></a></li>
	<li><a href="#"><?php echo lang('msg_users'); ?></a></li>
	<li class="active"><?php echo lang('msg_update_profile'); ?></li>
	
  </ol>
</section>

<section class="content">
    <!--show alert messager-->
    <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"><?php echo lang('msg_update_profile'); ?></h3>
        </div>
		
	<!--show alert messager-->	
	@if(isset($data['error_msg']))
	<div class="alert alert-danger">{{$data['error_msg']}}</div>
	@endif

	@if(isset($data['success_msg']))
	<div class="alert alert-success">{{$data['success_msg']}}</div>
	@endif

	<!--end show alert messager-->
	<form class="form-horizontal" id="form" method="post" action="" enctype="multipart/form-data">
		<fieldset>
			<input type="hidden" name="id" id="id" value="{{$data['obj'][0]->id}}">

			<div class="form-group">
				<label class="control-label col-md-2" for="txtName">{{lang('msg_old_pwd')}}</label>
				<div class="col-md-10">
					<input type="password" id="old_pwd" class="form-control" name="old_pwd"  value="{{set_value('old_pwd')}}">
					{{form_error('old_pwd')}}
				</div>
			</div>


			<div class="form-group">
				<label class="control-label col-md-2" for="txtName">{{lang('msg_new_pwd')}}</label>
				<div class="col-md-10">
					<input type="password" id="new_pwd" class="form-control" name="new_pwd" value="{{set_value('new_pwd')}}">
					{{form_error('new_pwd')}}
				</div>
			</div>


			<div class="form-group">
				<label class="control-label col-md-2" for="txtName">{{lang('msg_cfm_pwd')}}</label>
				<div class="col-md-10">
					<input type="password" id="cfm_pwd" class="form-control" name="cfm_pwd" value="{{set_value('cfm_pwd')}}">
					{{form_error('cfm_pwd')}}
				</div>
			</div>


			<div class="form-group">
				<div class="col-md-offset-2 col-md-10">
					<button type="submit" class="btn btn-primary" >
						{{lang('msg_save')}}
					</button>
				</div>
			</div>
		</fieldset>
	</form>
	<!--end form-->
</div>
<!--end container fluid-->
</section>
@endsection