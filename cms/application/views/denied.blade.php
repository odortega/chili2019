<!doctype html>
<html>
<head>
	@include('backends/commons/header')
</head>
<body>
	<div class="row">
		<div class="alert alert-error">
			You have no right permision to access this page, go  <a href="{{base_url().'admin/dashboard'}}">back</a> to admin panel or <a href="{{base_url().'admin/dashboard/logout'}}">logout</a>
		</div>
	</div>
	<style type="text/css">
	.alert{
		text-align: center;
		margin-top: 10px;
		margin-left: 50px;
		margin-right: 20px;
	}
	</style>
</body>
</html>