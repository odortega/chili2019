<?php
function send_verified_sms($verified_code,$phone){
	$CI =& get_instance();
	$CI->load->helper('settings');
	$configs = getSettings();
	//$phone = '841252655035';

	$subject = $CI->lang->line('verified_code_label');
	$body =$CI->lang->line('verified_msg').' '.$verified_code;

	if($configs['sms_prefer']=='0'){
		//if send by elastices sms
		$url ='https://api.elasticemail.com/sms/send?api_key='.$configs['elastice_api_key'].'&to=+'.$phone.'&body='.urlencode($body);
		try{
			$ch = curl_init();  
			curl_setopt($ch,CURLOPT_URL,$url);
			curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
			$output=curl_exec($ch);
			curl_close($ch);
			echo $output;
		}
		catch(Exception $ex){
			echo $ex->getMessage();
		}
	}

	if($configs['sms_prefer']=='1'){
		//if send by nexmo sms
		$credentials = new Nexmo\Client\Credentials\Basic($configs['nexmo_api_key'],$configs['nexmo_secret_key']);
		$client = new Nexmo\Client($credentials);
		$message = $client->message()->send([
			'from' => 'ChiliCMS',
			'to' => $phone,
			'text' => $body
			]);
	}
}

?>