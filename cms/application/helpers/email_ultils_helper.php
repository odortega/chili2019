<?php
function send_verified_mail($verified_code,$receive_email){
	$CI =& get_instance();
	$CI->load->helper('settings');
	$configs = getSettings();

	$subject = $CI->lang->line('verified_code_label');
	$body ='<p>'.$CI->lang->line('verified_msg').'&nbsp;<b>'.
	$verified_code.'</b><p>';


	if($configs['mail_prefer']=='0'){
		//if send by smtp
		//$configs['smtp_debug']=2;
		$CI->load->library('email',$configs);

		$CI->email->initialize($configs);
		$result = $CI->email
		->from($configs['from_email'],$configs['from_user'])
		->to($receive_email)
		->subject($subject)
		->message($body)
		->send();
	}

	if($configs['mail_prefer']=='1'){
		//if send by elastices search
		$url = 'https://api.elasticemail.com/v2/email/send';
		try{
			$post = array('from' => $configs['from_email'],
				'fromName' => $configs['from_user'],
				'apikey' => $configs['elastice_api_key'],
				'subject' => $subject,
				'to' => $receive_email,
				'bodyHtml' => $body,
				'bodyText' => $body,
				'isTransactional' => false);
			
			$ch = curl_init();
			curl_setopt_array($ch, array(
				CURLOPT_URL => $url,
				CURLOPT_POST => true,
				CURLOPT_POSTFIELDS => $post,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_HEADER => false,
				CURLOPT_SSL_VERIFYPEER => false
				));
			
			$result=curl_exec ($ch);
			curl_close ($ch);	
		}
		catch(Exception $ex){
			echo $ex->getMessage();
		}
	}
}




function send_enquiry($message,$receive_email="luann4099@gmail.com",$reply_to="beginlive@gmail.com",$user_name){
	$CI =& get_instance();
	$CI->load->helper('settings');
	$configs = getSettings();
	$CI->load->library('email',$configs);
	$CI->email->initialize($configs);
	$subject = $CI->lang->line('subject_enquiry_msg').' '.$user_name;
	$body ='<p>'.$user_name.'&nbsp;(<a href="mailto:'.$reply_to.'" target="_top">'.$reply_to.'</a>)&nbsp;'.$CI->lang->line('enquiry_msg').'<p>'.'<p>---------</p>'.$message.'<p>---------</p>'.$CI->lang->line('response_enquiry_msg');

	if($configs['mail_prefer']=='0'){
		//if send by smtp
		$result = $CI->email
		->from($configs['from_email'],$configs['from_user'])
		->to($receive_email)
		->subject($subject)
		->message($body)
		->reply_to($reply_to)
		->send();
	}


	if($configs['mail_prefer']=='1'){
		//if send by elastices search
		$url = 'https://api.elasticemail.com/v2/email/send';
		try{
			$post = array('from' => $configs['from_email'],
				'fromName' => $configs['from_user'],
				'apikey' => $configs['elastice_api_key'],
				'subject' => $subject,
				'to' => $receive_email,
				'bodyHtml' => $body,
				'bodyText' => $body,
				'replyTo'=>$reply_to,
				'isTransactional' => false);
			
			$ch = curl_init();
			curl_setopt_array($ch, array(
				CURLOPT_URL => $url,
				CURLOPT_POST => true,
				CURLOPT_POSTFIELDS => $post,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_HEADER => false,
				CURLOPT_SSL_VERIFYPEER => false
				));
			
			$result=curl_exec ($ch);
			curl_close ($ch);
		}
		catch(Exception $ex){
			echo $ex->getMessage();
		}
	}
}
?>