<?php
$CI =& get_instance();
$CI->load->helper('ultils');
$CI->load->helper('file');
function getSettings(){
	return object_2_array(json_decode(read_file(SETTINGS_FILE, true)));
}

$settings = array();
$settings = getSettings();
if(!$settings){ 
	//$settings['currency']='USD';

	/*Email settings*/
	$settings['useragent']        = 'PHPMailer';              // Mail engine switcher: 'CodeIgniter' or 'PHPMailer'
	$settings['protocol']         = 'smtp';                   // 'mail', 'sendmail', or 'smtp'
	$settings['smtp_timeout']     = 5;                        // (in seconds)
	$settings['smtp_crypto']      = '';                       // '' or 'tls' or 'ssl'
	$settings['smtp_debug']       = 0;                        // PHPMailer's SMTP debug info level: 0 = off, 1 = commands, 2 = commands and data
	$settings['wordwrap']         = true;
	$settings['wrapchars']        = 76;
	$settings['mailtype']         = 'html';                   // 'text' or 'html'
	$settings['charset']          = 'utf-8';
	$settings['validate']         = true;
	$settings['priority']         = 3;                        // 1, 2, 3, 4, 5
	$settings['crlf']             = "\n";                     // "\r\n" or "\n" or "\r"
	$settings['newline']          = "\n";                     // "\r\n" or "\n" or "\r"
	$settings['bcc_batch_mode']   = false;
	$settings['bcc_batch_size']   = 200;
	$settings['smtp_host']        = 'ssl://smtp.googlemail.com';
	$settings['smtp_user']        = '';
	$settings['smtp_pass']        = '';
	$settings['smtp_port']        = 465;
	$settings['from_email']       = '';
	$settings['from_user'] = 'ChiliCMS';
	$settings['mailpath']         = '';
	$settings['mail_prefer']='0';
	/*setting for elastic*/
	$settings['elastice_api_key']='';
	/*end setting for elastic*/

	/*setting for sms*/
	$settings['sms_prefer']='0';
	$settings['nexmo_api_key']='';
	$settings['nexmo_secret_key']='';
	/*end*/

	$settings['onesignal_app_id']='';
	$settings['onesignal_rest_key']='';


	/*end email settings*/

	$data = array();
	$json = json_encode($settings);
	write_file(SETTINGS_FILE, $json);
}

function reset_smtp_settings(){
	$settings=getSettings();
	$settings['mail_prefer']='0';
	$settings['useragent']        = 'PHPMailer';              // Mail engine switcher: 'CodeIgniter' or 'PHPMailer'
	$settings['protocol']         = 'smtp';                   // 'mail', 'sendmail', or 'smtp'
	$settings['smtp_timeout']     = 5;                        // (in seconds)
	$settings['smtp_crypto']      = '';                       // '' or 'tls' or 'ssl'
	$settings['smtp_debug']       = 0;                        // PHPMailer's SMTP debug info level: 0 = off, 1 = commands, 2 = commands and data
	$settings['wordwrap']         = true;
	$settings['wrapchars']        = 76;
	$settings['mailtype']         = 'html';                   // 'text' or 'html'
	$settings['charset']          = 'utf-8';
	$settings['validate']         = true;
	$settings['priority']         = 3;                        // 1, 2, 3, 4, 5
	$settings['crlf']             = "\n";                     // "\r\n" or "\n" or "\r"
	$settings['newline']          = "\n";                     // "\r\n" or "\n" or "\r"
	$settings['bcc_batch_mode']   = false;
	$settings['bcc_batch_size']   = 200;
	$settings['smtp_host']        = 'ssl://smtp.googlemail.com';
	$settings['smtp_user']        = '';
	$settings['smtp_pass']        = '';
	$settings['smtp_port']        = 465;
	$settings['mailpath']         = '';

	/*setting for elastic*/
	$settings['elastice_api_key']='';
	/*end setting for elastic*/

	$data = array();
	$json = json_encode($settings);
	write_file(SETTINGS_FILE, $json);
}

function reset_elastice_settings(){
	$settings=getSettings();
	/*setting for elastic*/
	$settings['elastice_api_key']='';
	/*end setting for elastic*/
	$data = array();
	$json = json_encode($settings);
	write_file(SETTINGS_FILE, $json);
}

function reset_general_mail_settings(){
	$settings=getSettings();
	$settings['from_email'] = '';
	$settings['from_user'] = 'ChiliCMS';
	$data = array();
	$json = json_encode($settings);
	write_file(SETTINGS_FILE, $json);
}

function reset_sms_settings(){
	$settings=getSettings();
	$settings['nexmo_api_key']='';
	$settings['nexmo_secret_key']='';
	$data = array();
	$json = json_encode($settings);
	write_file(SETTINGS_FILE, $json);
}

function setSettings($settings){
	$json=json_encode($settings);
	write_file(SETTINGS_FILE,$json);
}

function resetPush(){	
	$settings=getSettings();
	$settings['onesignal_app_id']='';
	$settings['onesignal_rest_key']='';
	$json=json_encode($settings);
	write_file(SETTINGS_FILE,$json);
}
?>
