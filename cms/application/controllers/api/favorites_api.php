<?php
require APPPATH.'/libraries/REST_Controller.php';
class favorites_api extends REST_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('favorites_model');
	}

	function favorites_get(){
		$first=$this->input->get('first');
		$offset=$this->input->get('offset');
		$where=array();
		$like=array();
		$order=array('favorites.created_at'=>'DESC');
		$where['users.activated']=1;
		$where['products.activated']=1;

		$product_id=$this->input->get('product_id');
		if($product_id != null && $product_id != 0){
			$where['favorites.product_id']=$product_id;
		}

		$user_id=$this->input->get('user_id');
		if($user_id != null && $user_id != 0){
			$where['favorites.user_id']=$user_id;
		}

		$id=$this->input->get('id');
		if($id != null && $id != 0){
			$where['favorites.id']=$id;
		}

		$select='*,
		favorites.created_at as created_at,
		favorites.updated_at as updated_at,
		favorites.product_id as id,
		favorites.user_id as user_id';

		$data=$this->favorites_model->get($select,$where,$like,$first,$offset,$order);
		if($data!=null){
			$this->response($data);
		}else{
			$this->response(array('empty'=>1));
		}
	}

	function favorites_by_user_id_get(){
		$user_id=$this->get('user_id');
		$data=$this->favorites_model->get_by_user_id($user_id);
		if($data!=null){
			$this->response($data); 
		}else{
			$this->response(array('empty'=>1));
		}
	}

	function favorites_by_product_id_get(){
		$product_id=$this->get('product_id');
		$data=$this->favorites_model->get_by_product_id($product_id);
		if($data!=null){
			$this->response($data); 
		}else{
			$this->response(array('empty'=>1));
		}
	}

	function favorites_by_product_and_user_get(){
		$product_id=$this->get('product_id');
		$user_id=$this->get('user_id');
		$data=$this->favorites_model->get_by_product_end_user($product_id, $user_id);
		if($data!=null){
			$this->response($data); 
		}else{
			$this->response(array('empty'=>1));
		}
	}

	function add_un_favorites_post(){
		$product_id = $this->post('product_id');
		$user_id = $this->post('user_id');
		$data['product_id'] = $product_id;
		$data['user_id'] = $user_id;
		$check = $this->favorites_model->get_by_product_and_user($product_id, $user_id);
		
		if($check == null){
			$this->favorites_model->insert($data);
			$this->response(array('favorites'=>true));
		}else{
			$this->favorites_model->remove($data);
			$this->response(array('favorites'=>false));
		}
	}

	function remove_all_post(){
		$data['user_id'] = $this->post('user_id');
		$this->favorites_model->remove($data);
	}
}
?>