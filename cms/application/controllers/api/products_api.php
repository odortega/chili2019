<?php
require APPPATH.'/libraries/REST_Controller.php';
class products_api extends REST_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('product_model');
		$this->load->helper('Ultils');
	}

	function products_get() 
	{ 
		$first=$this->input->get('first');
		$offset=$this->input->get('offset');
		$where=array();
		$like=array();
		$order=array('products.created_at'=>'DESC');
		$where['users.activated']=1;
		$where['products.activated']=1;
		$user_fv=null;

		$categories_id=$this->input->get('categories_id');
		if($categories_id!=null && $categories_id!=0){
			$where['FIND_IN_SET('.$categories_id.',products.categories_id)<>']=0;
		}
		
		$product_id=$this->input->get('product_id');
		if($product_id!=null && $product_id!=0){
			$where['products.id']=$product_id;
		}

		$sort_by=$this->input->get('sort_by');
		if($sort_by!=null){
			$order=array('products.updated_at'=>$sort_by);
		}

		$id=$this->input->get('pull');
		if($id!=null){
			$product=$this->product_model->get_by_id($id);
			$where['products.id <>']=$id;
			$where['products.created_at >=']= date('Y-m-d H:i:s',strtotime($product[0]->created_at));
		}

		$title=$this->input->get('title');
		if($title!=null){
			$like['title']=$title;
		}

		$min_price = (int)$this->input->get('min_price');
		$max_price = (int)$this->input->get('max_price');
		if(is_numeric($min_price) && $min_price != null && is_numeric($max_price) && $max_price != null){
			$where['products.price >='] = $min_price;
			$where['products.price <='] = $max_price;
		}

		$aim=$this->input->get('aim');
		if($aim!=null){
			$where['purpose']=$aim;
		}
		$select='*,
		products.created_at as created_at,
		products.updated_at as updated_at,
		categories.name as categories_name,
		products.id as id,
		products.user_id as user_id';

		$user_id=$this->input->get('user_id');
		if($user_id!=null){
			$where['products.user_id']=$user_id;
		}
		
		$user_lg=$this->input->get('user_lg');
		$data=$this->product_model->get($select,$where,$like,$first,$offset,$order,$user_lg);
		if($data!=null){
			$this->response($data);
		}else{
			$this->response(array('empty'=>1));
		}
	}



	function slider_get(){
		$select='*,
		products.created_at as created_at,
		products.updated_at as updated_at,
		categories.name as categories_name,
		products.id as id,
		products.user_id as user_id';
		$order=array('products.id'=>'DESC');
		$like=array();
		$user_lg=$this->input->get('user_lg');
		$data=$this->product_model->get($select,array('products.is_slider'=>1, 'products.activated'=>1),null,0,10,$order,$user_lg);
		if($data!=null){
			$this->response($data);
		}else{
			$this->response(array('empty'=>1));
		}
	}

	function products_post() 
	{ 
		$title=preg_replace('/[\r\n]+/', "", $this->post('title')); 
		$content=preg_replace('/[\r\n]+/', "", $this->input->post('content'));
		$price=$this->post('price');
		$categories=$this->post('cat_id');
		$fb_id=$this->post('fb_id');
		$user_id=$this->post('user_id');
		$location =$this->post('location');
		$purpose= $this->post('purpose');
		$condition=$this->post('condition');
		$lat=$this->post('lat');
		$lng=$this->post('lng');
		$images=array();
		$data=array(
			'title'=>$title,
			'price'=>$price,
			'content'=>$content,
			'categories_id'=>$categories,
			'fb_id'=>$fb_id,
			'user_id'=>$user_id,
			'location'=>$location,
			'purpose'=>$purpose,
			'cond'=>$condition,
			'lat'=>$lat,
			'lng'=>$lng
		);

		$insert_id = $this->product_model->insert($data);
		$image_path=null;
		$thumb=null;

		if(isset($_FILES['file'])){
			$config['upload_path'] = 'uploads/products/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg|JPG|JPEG|GIF|PNG';
			$config['max_size']	= '15000';
			$this->load->library('upload', $config);
			$filename=$_FILES['file']['name'];
			$_FILES['file']['name']=rename_upload_file($filename);	
			if ($this->upload->do_upload('file'))
			{
				$image_path='uploads/products/'.$_FILES['file']['name'];
				$thumb=$_FILES['file']['name'];
			}
		}

		if($insert_id!=0){
			if($image_path!=null){
				$config=array(
                            "source_image" => $image_path, //get original image
							"new_image" =>  "uploads/products/thumbs", //save as new image //need to create thumbs first
							"maintain_ratio" => true,
							"width" => 500,
							"height" => 500
						);
				$this->load->library('image_lib',$config);
				$this->image_lib->resize();
				$thumb= 'uploads/products/thumbs/'.$thumb;
				$this->product_model->update(array('image_path'=>$thumb), array('id'=>$insert_id));	
				$this->load->model('images_model');
				$this->images_model->insert(array('path'=>$image_path,'product_id'=>$insert_id));
			}
			$this->response(array('ok'=>'1'));
		}else{
			$this->response(array('ok'=>'0'));
		}
	} 

	function products_put() 
	{ 
		$data = array('this not available'); 
		$this->response($data); 
	} 

	function products_delete() 
	{ 
		$data = array('this not available');
		$this->response($data); 
	}  

	function update_post(){
		if(isset($_POST['product_id'])){
			$product_id=$this->input->post('product_id');
			$data=array();
			$data['title']=$this->input->post('title');
			$data['content']=$this->input->post('content');
			//$data['cities_id']=$this->input->post('cities');
			//$data['county_id']=$this->input->post('county');
			$data['price']=$this->input->post('price');
			//$data['condition']=$this->input->post('condition');
			//$data['aim']=$this->input->post('aim');
			$this->product_model->update($data,array('id'=>$product_id));
		}
	}

	function report_post(){
		$this->load->model('report_model');
		$user_id=$this->post('user_id');
		$product_id=$this->post('product_id');
		$check=$this->report_model->get_by_product_and_user($product_id, $user_id);
		if($check == null){
			$this->report_model->insert(array('product_id'=>$product_id,'user_id'=>$user_id));
			$temp = $this->report_model->get_by_product_and_user($product_id, $user_id)[0];
			$this->product_model->update(array('report'=>$temp->report + 1),array('id'=>$product_id));
			if($temp->report + 1 >= 10){
				$this->product_model->update(array('activated'=>0),array('id'=>$product_id));
			}
			$this->response(array('report'=>true));
		}else{
			$this->report_model->remove(array('product_id'=>$product_id,'user_id'=>$user_id));
			$this->product_model->update(array('report'=>$check[0]->report - 1),array('id'=>$product_id));
			$this->response(array('report'=>false));
		}
	}

	function delete_post(){
		if(isset($_POST['id'])){
			$id=$this->input->post('id');
			$product=$this->product_model->get_by_id($id);
			if($product!=null){
				$this->load->model('images_model');
				$images=$this->images_model->get_by_product_id($id);
				foreach ($images as $r) {
					try {
						unlink($r->path);
						$this->images_model->remove_by_id($r->id);
					} catch (Exception $e) {
						
					}
				}
				try {
					unlink($product[0]->image_path);
				} catch (Exception $e) {
					
				}
			}
			$this->product_model->remove_by_id($id);
		}
	}

	function rate_post(){
		if(isset($_POST['product_id'])){
			$product_id=$this->input->post('product_id');
			$user_id=$this->input->post('user_id');
			$product_user_id=$this->input->post('product_user_id');
			$point=$this->input->post('point');
			$message=$this->input->post('message');
			$this->load->model('ratings_model');
			$check=$this->ratings_model->get_by_user_id_and_product_id($user_id,$product_id);
			if($check!=null){
				$data=array();
				$data['product_id']=$product_id;
				$data['user_id']=$user_id;
				$data['product_user_id']=$product_user_id;
				$this->ratings_model->update(array('point'=>$point),$data);
			}else{
				$data=array();
				$data['product_id']=$product_id;
				$data['user_id']=$user_id;
				$data['product_user_id']=$product_user_id;
				$data['point']=$point;
				$data['comment']=$message;
				$this->ratings_model->insert($data);
			}
		}
	}

	
	function nearby_get(){
		$lat=$this->input->get('lat');
		$lng=$this->input->get('lng');
		$radius=$this->input->get('radius');
		$title=$this->input->get('title');
		$first=$this->input->get('first');
		$offset=$this->input->get('offset');

		$min_price=$this->input->get('min_price');
		$max_price=$this->input->get('max_price');

		$categories_id=$this->input->get('categories_id');

		$having=' ';
		if(is_numeric($radius)){
			$radius;
			$having=' HAVING distance < '.$radius;
		}

		$where = '`products`.`activated`=1';
		if($title!=null && $title!=''){
			$where.=' AND title LIKE "%'.$title.'%"';
		}

		if(is_numeric($min_price)){
			$where .= ' AND `products`.price >'.$min_price;
		}

		if(is_numeric($max_price)){
			$where .= ' AND `products`.price <'.$max_price;
		}

		if(is_numeric($categories_id)){
			$where .= ' AND `products`.categories_id ='.$categories_id;
		}

		$select='SELECT *,ROUND(6371 * acos( cos( radians('.$lat.') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians('.$lng.') ) + sin( radians('.$lat.') ) * sin( radians( lat ) ) ) )
		AS distance
		FROM products
		JOIN users ON products.user_id = users.id
		WHERE '.$where.$having.' ORDER BY distance ASC LIMIT '.$first.' , '.$offset;

		//echo $select;

		$data=$this->product_model->get_by_query($select);
		if($data!=null){
			$this->response($data);
		}else{
			$this->response();
		}
	}

}
?>