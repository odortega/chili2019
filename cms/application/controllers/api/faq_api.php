<?php
require APPPATH.'/libraries/REST_Controller.php';
class faq_api extends REST_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('faq_model');
	}

	function faq_get(){ 
		$first=$this->get('first');
		$offset=$this->get('offset');
		$data=$this->faq_model->get('*',array(),array(),$first,$offset,array('id'=>'DESC'));
		if($data!=null){
			$this->response($data); 
		}else{
			$this->response(array('empty'=>'empty_data'));
		}
	} 
}
?>