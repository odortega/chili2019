<?php
require APPPATH.'/libraries/REST_Controller.php';
class images_api extends REST_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('images_model');
	}

	function images_get() 
	{ 
		$where=array();
		$like=array();
		$order=array('images.id'=>'DESC');
		$data=$this->images_model->get();
		$product_id=$this->get('product_id');
		if($product_id!=null){
			$where['product_id']=$product_id;
		}
		$data=$this->images_model->get("*", $where, $like, false, false, $order);
		if($data!=null){
			$this->response($data); 
		}else{
			$this->response(array('empty'=>'empty_data'));
		}
	} 

	function images_post()
	{
		$this->load->helper('ultils');

		if(isset($_FILES['file']) && isset($_POST['id'])){
			$id=$this->input->post('id');
			$dir=create_dir_upload('uploads/products/');
			$thumb_dir= create_thumb_dir_upload($dir.'/thumbs');
			$filename=$_FILES['file']['name'];
			$_FILES['file']['name']=rename_upload_file($filename);
			$config['allowed_types'] = 'gif|jpg|png|jpeg|JPG|JPEG|GIF|PNG';
			$config['max_size']	= '5000';
			$config['upload_path']=$dir;
			$this->load->library('upload',$config);
			if ($this->upload->do_upload('file'))
			{
				$this->load->model('images_model');
				$data['path']=$dir.'/'.$_FILES['file']['name'];
				$data['product_id']=$id;
				$original_path=$data['path'];
				$images_id = $this->images_model->insert($data);

				$config=array(
              				"source_image" => $dir.'/'.$_FILES['file']['name'], //get original image
							"new_image" =>  $thumb_dir, //save as new image //need to create thumbs first
							"width" => 270,
							"height" => 200,
							'master_dim'=>'height'
							);
				$this->load->library('image_lib',$config);
				$this->image_lib->resize();
				$image_path= $thumb_dir.'/'.$_FILES['file']['name'];
				$data=null;
				$data['thumb_path']=$image_path;
				$this->images_model->update($data,array('id'=>$images_id));
				$return_data=array(
					'ok'=>1,
					'thumb_path'=>$image_path,
					'product_id'=>$id,
					'id'=>$images_id,
					'path'=>$original_path
					);
				$this->response($return_data);
			}
			else
			{
				$this->response(array('success'=>0));
			}
		}
	}

	function images_put() 
	{ 
		$data = array('this not available'); 
		$this->response($data); 
	} 

	function images_delete() 
	{ 
		$data = array('this not available');
		$this->response($data); 
	}  
}
?>