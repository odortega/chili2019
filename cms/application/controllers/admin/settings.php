<?php
class Settings extends CI_Controller{
	function __construct()
	{
		parent::__construct();
		if(!isset($_SESSION['user'])){
			redirect('admin/dashboard');
		}else{
			$user=$_SESSION['user'][0];
			if($user->perm==USER){
				redirect('admin/denied');
			}
		}
		$this->load->helper('settings');
		$this->form_validation->set_error_delimiters('<div class="error-line">', '</div>');
	}

	function currency(){
		if(isset($_POST['currency'])){
			$currency=$this->input->post('currency');
			$settings=getSettings();
			$settings['currency']=$currency;
			setSettings($settings);
		}
		$this->load->model('countries_model');
		$data['list']=$this->countries_model->get_currency_code();
		$this->blade->render('backends/settings/currency',$data);
	}

	function mail(){
		if(isset($_POST['from_user'])){
			$from_user=$this->input->post('from_user');
			$from_email=$this->input->post('from_email');
			$settings=getSettings();
			$settings['from_email'] = $from_email;
			$settings['from_user'] = $from_user;
			setSettings($settings);
		}

		//if smtp setting
		if(isset($_POST['host'])){
			$host=$this->input->post('host');
			$user=$this->input->post('user');
			$pwd=$this->input->post('pwd');
			$port=$this->input->post('port');
			$mailpath=$this->input->post('mail_path');
			$settings=getSettings();
			$settings['smtp_host']        = $host;
			$settings['smtp_user']        = $user;
			$settings['smtp_pass']        = $pwd;
			$settings['smtp_port']        = $port;
			$settings['mailpath']         = $mailpath;

			setSettings($settings);
		} 

		//if elastice setting
		if(isset($_POST['elastice_api_key'])){
			$settings=getSettings();
			$settings['elastice_api_key']=$this->input->post('elastice_api_key');
			setSettings($settings);
		}
		
		$data['obj']=getSettings();
		$this->blade->render('backends/settings/email',$data);
	}

	function mail_prefer_settings(){
		if(isset($_GET['prefer'])){
			$settings=getSettings();
			$prefer=$this->input->get('prefer');
			$settings['mail_prefer']=$prefer;   
			setSettings($settings);
			redirect('admin/settings/mail');
		}
	}

	function reset_smtp_settings(){
		reset_smtp_settings();
		redirect('admin/settings/mail');
	}

	function reset_elastice_settings(){
		reset_elastice_settings();
		redirect('admin/settings/mail');
	}

	function reset_general_mail_settings(){
		reset_general_mail_settings();
		redirect('admin/settings/mail');
	}

	function sms(){
		//if elastice setting
		if(isset($_POST['elastice_api_key'])){
			$settings=getSettings();
			$settings['elastice_api_key']=$this->input->post('elastice_api_key');
			setSettings($settings);
		}

		if(isset($_POST['nexmo_api_key'])){
			$settings=getSettings();
			$settings['nexmo_api_key']=$this->input->post('nexmo_api_key');
			$settings['nexmo_secret_key']=$this->input->post('nexmo_secret_key');
			setSettings($settings);
		}		
		$data['obj']=getSettings();
		$this->blade->render('backends/settings/sms',$data);
	}

	function sms_prefer_settings(){
		if(isset($_GET['prefer'])){
			$settings=getSettings();
			$prefer=$this->input->get('prefer');
			$settings['sms_prefer']=$prefer;   
			setSettings($settings);
			redirect('admin/settings/sms');
		}
	}

	function reset_elastice_settings_2(){
		reset_elastice_settings();
		redirect('admin/settings/sms');
	}

	function reset_sms_settings(){
		reset_sms_settings();
		redirect('admin/settings/sms');
	}


	function push(){
		if(isset($_POST['app_id'])){
			$app_id=$this->input->post('app_id');
			$rest_key=$this->input->post('rest_key');

			$settings=getSettings();
			$settings['onesignal_app_id']        = $app_id;
			$settings['onesignal_rest_key']        = $rest_key;
			setSettings($settings);
		}
		$data['obj']=getSettings();
		$this->blade->render('backends/settings/push',$data);
	}

	function reset_push(){
		resetPush();
		redirect('admin/settings/push');
	}

}
?>