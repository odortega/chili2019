<?php
class Home extends CI_Controller{
	function __construct()
	{
		parent::__construct();
	}

    function index(){
    	redirect(base_url().'admin/dashboard');
    }

    function detail(){
        $this->load->model('product_model');
        $this->load->model('images_model');
        $id = $this->input->get('id');
        $data['imgs'] = $this->images_model->get_by_product_id($id);
        $data['obj'] = $this->product_model->get_by_id($id);
        $data['obj'] = $data['obj'][0];
        $this->blade->render('frontends/index', $data);
    }
}
?>