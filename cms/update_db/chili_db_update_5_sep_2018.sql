ALTER TABLE `products` CHANGE COLUMN `condition` `cond` tinyint(4) DEFAULT '0';
ALTER TABLE `products` CHANGE COLUMN `aim` `purpose` tinyint(4) DEFAULT NULL;
ALTER TABLE `products` ADD COLUMN location varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL;